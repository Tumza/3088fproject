EESchema Schematic File Version 4
LIBS:EEE3088F-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7450 6950 0    79   ~ 16
EEE3088F Project Hand-in 3\nItumeleng Seeco
Text Notes 7350 7500 0    59   ~ 12
Input Signal Module
Text Notes 8250 7650 0    59   ~ 12
03 June 2021
$Comp
L Device:R R1
U 1 1 606BAD55
P 5500 3150
F 0 "R1" V 5293 3150 50  0000 C CNN
F 1 "10k" V 5384 3150 50  0000 C CNN
F 2 "" V 5430 3150 50  0001 C CNN
F 3 "~" H 5500 3150 50  0001 C CNN
	1    5500 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 606BB335
P 5500 4050
F 0 "R2" V 5293 4050 50  0000 C CNN
F 1 "18k" V 5384 4050 50  0000 C CNN
F 2 "" V 5430 4050 50  0001 C CNN
F 3 "~" H 5500 4050 50  0001 C CNN
	1    5500 4050
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U2
U 1 1 606C17CC
P 6300 3500
F 0 "U2" H 6300 3867 50  0000 C CNN
F 1 "LM358" H 6300 3776 50  0000 C CNN
F 2 "" H 6300 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 6300 3500 50  0001 C CNN
	1    6300 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3600 6000 3600
Wire Wire Line
	6600 3500 6600 4000
Wire Wire Line
	6600 3500 7100 3500
Connection ~ 6600 3500
Text Label 7100 3500 0    50   ~ 0
GPIO_5
$Comp
L power:+5V #PWR04
U 1 1 6075E0A0
P 5500 2850
F 0 "#PWR04" H 5500 2700 50  0001 C CNN
F 1 "+5V" H 5515 3023 50  0000 C CNN
F 2 "" H 5500 2850 50  0001 C CNN
F 3 "" H 5500 2850 50  0001 C CNN
	1    5500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2850 5500 3000
$Comp
L power:GND #PWR?
U 1 1 60B99952
P 5500 4450
F 0 "#PWR?" H 5500 4200 50  0001 C CNN
F 1 "GND" H 5505 4277 50  0000 C CNN
F 2 "" H 5500 4450 50  0001 C CNN
F 3 "" H 5500 4450 50  0001 C CNN
	1    5500 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4450 5500 4200
Wire Wire Line
	5500 3900 5500 3400
Wire Wire Line
	5800 3600 5800 4000
Wire Wire Line
	5800 4000 6600 4000
Wire Wire Line
	5500 3400 6000 3400
Connection ~ 5500 3400
Wire Wire Line
	5500 3400 5500 3300
Wire Wire Line
	4700 3400 5500 3400
Text Label 4700 3400 0    50   ~ 0
HEADER_PIN
$EndSCHEMATC
